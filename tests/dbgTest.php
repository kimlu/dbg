<?php
declare( strict_types=1 );

namespace tests;

use PHPUnit\Framework\TestCase;
use dbg;

class dbgTest extends TestCase
{
    /**
     * 
     * @var string|NULL
     */
    private ?string $filepath = NULL;
    
    /**
     * 
     * {@inheritDoc}
     * @see \PHPUnit\Framework\TestCase::setUp()
     */
    public function setUp(): void
    {
        $filepath = __DIR__.DIRECTORY_SEPARATOR.'file.log';
        if( !file_exists( $filepath ) )
        {
            touch( $filepath );
        }
        $this->filepath = realpath( $filepath );
        dbg::activateLog( $filepath );
    }
    
    /**
     * 
     * {@inheritDoc}
     * @see \PHPUnit\Framework\TestCase::tearDown()
     */
    public function tearDown(): void
    {
        file_put_contents( $this->filepath, '' );
    }
    
    /**
     * Testing dbg::log
     */
    public function testLog(): void
    {
        dbg::log( $_SERVER );
        
        $this->assertTrue( file_exists( $this->filepath ) );
        $this->assertTrue( filesize( $this->filepath ) > 0 );        
    }

    /**
     * Testing dbg::html
     */
    public function testHtml(): void
    {
        $codeExecuted = '';
        ob_start();
        dbg::html( $_SERVER );
        $codeExecuted = ob_get_contents();
        ob_end_clean();
        
        $codeExpected = '<pre style="white-space: pre-wrap; word-break: keep-all">';
        $codeExpected .= dbg::processValue( $_SERVER );
        $codeExpected .= '</pre>';    
        
        $this->assertEquals( $codeExpected, $codeExecuted );        
    }

}