<?php

abstract class dbg
{

    /**
     * 
     * @param string $errorLogPath
     */
    static public function activateLog( string $errorLogPath = NULL ): void
    {
        ini_set( 'log_errors', TRUE  );
        
        if ( isset( $errorLogPath ) )
        {
            ini_set( 'error_log', $errorLogPath );
        }
        
        ini_set( 'display_errors', 'stderr' );
        
        error_reporting( E_ALL );
    }
    
    
    /**
     * 
     * @param mixed $value
     */
    static public function log( $value ): void
    {
        $sources = PHP_EOL;
        $sources .= self::processValue( $value );
        $sources .= PHP_EOL;
        error_log( $sources );
    }
    
    /**
     * 
     * @param mixed $value
     */
    static public function html( $value ): void
    {
        print '<pre style="white-space: pre-wrap; word-break: keep-all">';
        print self::processValue( $value );
        print '</pre>';        
    }
    
    /**
     * 
     * @param mixed $value
     * @return string
     */
    static public function processValue( $value ): string
    {
        return var_export( $value, TRUE );
    }
    
}